package python;

import java.io.File;
import java.io.IOException;

import guru.nidi.graphviz.engine.Format;
import guru.nidi.graphviz.engine.Graphviz;

public class CallPythonFunction {
	public static void main(String[] args) throws IOException {
//		try {
//			PythonInterpreter.initialize(System.getProperties(), System.getProperties(),
//					new String[] { "-c", "1", "--author", "sahar badihi", "--phrase", "software" });
//			PythonInterpreter interpreter = new PythonInterpreter();
//			interpreter.execfile("scholar.py");
//		} catch (Exception e) {
//
//		}
		
        String dotFile =
                new info.leadinglight.jdot.Graph("MyGraph")
                        .addNodes(
                            new info.leadinglight.jdot.Node().setShape(info.leadinglight.jdot.enums.Shape.record),
                            new info.leadinglight.jdot.Node("node1").setShape(info.leadinglight.jdot.enums.Shape.record).setLabel("Node 1"),
                            new info.leadinglight.jdot.Node("node2").setShape(info.leadinglight.jdot.enums.Shape.record).setLabel("Node 2"),
                            new info.leadinglight.jdot.Node("node3").setShape(info.leadinglight.jdot.enums.Shape.record).setLabel("Node 3"))
                        .addEdges(
                                new info.leadinglight.jdot.Edge().addNode("node1", "N1").addNode("node2", "N2").setDir(info.leadinglight.jdot.enums.Dir.none),
                                new info.leadinglight.jdot.Edge().addNode("node1", "N1").addNode("node3", "N3").setDir(info.leadinglight.jdot.enums.Dir.none))
                        .toDot();
        

        Graphviz
                .fromString(dotFile)
                .width(900)
                .render(Format.PNG)
                .toFile(new File("graph/graph.png"));
	}
}
